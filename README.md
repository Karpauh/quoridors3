# PROJET QUORIDOR

### Description

Ceci est un dépôt GIT d'un jeu QUORIDOR réalisé dans le cadre d'un projet d'IUT (2e année), réalisé par 4 personnes.

### Installer et tester le jeu

Vous pouvez tester le jeu de chez vous facilement. Clonez le dépôt git et **compilez** la classe "App.java", le jeu se lance.

Version Java utilisée: **Java 8**.

Suite d'instructions sur linux

- se rendre dans le paquet fr.umontpellier
- javac App.java
- java ./App



