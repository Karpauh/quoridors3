package fr.umontpellier.iut;

import fr.umontpellier.iut.Affichage;
import fr.umontpellier.iut.Mur;
import fr.umontpellier.iut.Partie;
import fr.umontpellier.iut.Position;
import fr.umontpellier.iut.IA;

public class IANiv1 extends IA {

    private Mur murAPlacer;
    private Position posToGo;


    public IANiv1(int ligne, int col, Partie partie) {
        super(ligne, col, partie);
        this.partie = partie;
        this.murAPlacer = new Mur(new Position(-1, -1), "h");
        this.posToGo = new Position(0,0);
    }





    @Override
    public String choisirAction(Affichage affichage) {
        String choix = "d";

        int distanceJCourrant = distToWin(partie.getJoueurCourant(), this, partie.getPlateau());

        int distanceJAutre = distToWin((partie.getJoueurSuivant()), partie.getListeJoueurs()[(partie.getJoueurSuivant())], partie.getPlateau());
        System.out.println("test");

        System.out.println("jCourrant : "+distanceJCourrant+" jAutre : "+distanceJAutre);
        if (distanceJAutre<distanceJCourrant) {
            choix = "m";
        }
        posToGo = posaaller(partie.getJoueurCourant(),this, partie.getPlateau());
        affichage.afficher(choix);
        return choix;
    }

    @Override
    public Position choisirMouvement(Affichage affichage) {
        System.out.println("j1 : "+distToWin(0, partie.getListeJoueurs()[0], partie.getPlateau())+" j2 : "+distToWin(1, partie.getListeJoueurs()[1], partie.getPlateau()));

        return posaaller(partie.getJoueurCourant(), partie.getListeJoueurs()[partie.getJoueurCourant()], partie.getPlateau());
    }



    private void determinerMurAPlacer() {
        Position posJoueurAdverse = new Position(partie.getListeJoueurs()[(partie.getJoueurSuivant())].getPosition());
        String dirOptiAdverse = directionPos(posaaller((partie.getJoueurSuivant()), partie.getListeJoueurs()[(partie.getJoueurSuivant())], partie.getPlateau()), partie.getListeJoueurs()[partie.getJoueurSuivant()].getPosition(), partie.getPlateau());
        String orientation;
        Position posMurAPlacer;
        switch (dirOptiAdverse) {
            case "HAUT":
                System.out.println("àaaaa"+posJoueurAdverse.getLigne());
                posMurAPlacer = new Position(posJoueurAdverse.getLigne()-1, posJoueurAdverse.getColonne());
                orientation = "h";break;
            case "GAUCHE":
                posMurAPlacer = new Position(posJoueurAdverse.getLigne(), posJoueurAdverse.getColonne()-1);
                orientation = "v";break;
            case "DROITE":
                posMurAPlacer = new Position(posJoueurAdverse.getLigne(), posJoueurAdverse.getColonne());
                orientation = "v";break;
            default:
                posMurAPlacer = new Position(posJoueurAdverse.getLigne(), posJoueurAdverse.getColonne());
                orientation = "h";break;
        }
        System.out.println(dirOptiAdverse+" : "+posMurAPlacer.getLigne()+" "+posMurAPlacer.getColonne());

        boolean valide = false;

        valide = partie.getPlateau().murEstValide(posMurAPlacer.getLigne(), posMurAPlacer.getColonne(), orientation);

        if (!valide) {
            orientation = "v";
            valide = partie.getPlateau().murEstValide(posMurAPlacer.getLigne(), posMurAPlacer.getColonne(), orientation);
        }

        if (!valide) {
            //parcours simple
            int taille = partie.getPlateau().getTaille();
            int ligne = 0;
            int colonne = 0;
            orientation = "v";
            valide = partie.getPlateau().murEstValide(ligne, colonne, orientation);
            while(!valide && ligne<taille-1) {
                while (!valide && colonne < taille - 1) {
                    if (orientation.equals("v")) {
                        orientation = "h";
                    } else {
                        orientation = "v";
                    }
                    valide = partie.getPlateau().murEstValide(ligne, colonne, orientation);
                    if (!valide) {
                        colonne++;
                        valide = partie.getPlateau().murEstValide(ligne, colonne, orientation);
                    }
                }
                if (!valide) {
                    colonne = 0;
                    ligne++;
                }
            }
            if (!valide) {
                System.out.println("Plus de place pour des murs :(");
            }
            murAPlacer = new Mur(new Position(ligne, colonne), orientation);
        }
        else {

            murAPlacer = new Mur(posMurAPlacer, orientation);
        }
        delayer();
    }

    @Override
    public int choisirLigneMur(Affichage affichage, int taille) {
        determinerMurAPlacer();
        int choix = murAPlacer.getPosition().getLigne();
        affichage.afficher(""+choix);
        return choix;
    }

    @Override
    public int choisirColonneMur(Affichage affichage, int taille) {
        determinerMurAPlacer();
        int choix = murAPlacer.getPosition().getColonne();
        affichage.afficher(""+choix);
        return choix;
    }

    @Override
    public String choisirOrientationMur(Affichage affichage) {
        determinerMurAPlacer();
        String choix = murAPlacer.getOrientation().toLowerCase();
        affichage.afficher(choix);
        return choix;
    }




}
