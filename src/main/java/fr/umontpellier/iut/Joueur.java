package fr.umontpellier.iut;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Joueur {
    private int nbMur;
    private Position position;

    //Creation du joueur avec une position (ligne, colonne) et possede 10 murs
    public Joueur(int ligne, int col) {
        this.position = new Position(ligne, col);
        this.nbMur = 10;
    }

    public Joueur(Position position, int nbMur) {
        this.position = new Position(position);
        this.nbMur = nbMur;
    }

    public Position getPosition(){ return this.position; }

    public void seDeplacer(Position pos){
        this.position = pos;
    }

    public void poserMur() {
        nbMur--;
    }

    public int getNbMur() {
        return nbMur;
    }

    public String choisirAction(Affichage affichage) {
        System.out.println("Choix de l'action : se Déplacer (d) ou poser un mur (m)");
        String choix = affichage.getStringInput("Choix(d/m)");
        while (!choix.equals("d") && !choix.equals("m")) {
            System.out.println("Choix invalide");
            System.out.println("Choix de l'action : se Déplacer (d) ou poser un mur (m)");
            choix = affichage.getStringInput("Choix(d/m)");
        }
        return choix;
    }
    public Position choisirMouvement(Affichage affichage) {
        int l, c;
        l = affichage.getIntInput("Ligne");
        c = affichage.getIntInput("Colonne");
        System.out.println("Se deplacer : entrer une coordonnée");
        Position pos = new Position(l,c);
        return pos;
    }

    public int choisirLigneMur(Affichage affichage, int taille) {
        return affichage.getIntInput("Choix(0.." + (taille - 2) + ")");
    }
    public int choisirColonneMur(Affichage affichage, int taille) {
        return affichage.getIntInput("Choix(0.." + (taille - 2) + ")");
    }
    public String choisirOrientationMur(Affichage affichage) {
        return affichage.getStringInput("Choix(v/h)");
    }


}