package fr.umontpellier.iut;

public class Case {
    private Position position;
    private Case[] listeVoisin; //HAUT : 0; BAS : 1; GAUCHE : 2; DROITE : 3


    public Position getPosition(){ return this.position; }

    /*Construction d'une case avec un indice de ligne et de colonne (elle se place donc dans une matrice)*/
    public Case(int indiceLigne, int indiceCol) {
        this.position = new Position(indiceLigne, indiceCol);
        listeVoisin = new Case[4];
    }

    public void setVoisin(String direction, Case voisin) throws IllegalArgumentException{
        direction = direction.toUpperCase();
        switch (direction) {
            case "HAUT": listeVoisin[0] = voisin; break;
            case "BAS": listeVoisin[1] = voisin; break;
            case "GAUCHE": listeVoisin[2] = voisin; break;
            case "DROITE": listeVoisin[3] = voisin; break;
            default: throw new IllegalArgumentException("Direction invalide");
        }
    }

    public Case getVoisin(String direction) {
        direction = direction.toUpperCase();
        switch (direction) {
            case "HAUT": return listeVoisin[0];
            case "BAS": return listeVoisin[1];
            case "GAUCHE": return listeVoisin[2];
            case "DROITE": return listeVoisin[3];
            default: throw new IllegalArgumentException("Direction invalide");
        }
    }

    public boolean aUnMur(String direction) {
        direction = direction.toUpperCase();
        switch (direction) {
            case "HAUT": return listeVoisin[0]==null;
            case "BAS": return listeVoisin[1]==null;
            case "GAUCHE": return listeVoisin[2]==null;
            case "DROITE": return listeVoisin[3]==null;
            default: throw new IllegalArgumentException("Direction invalide");
        }
    }

    public void supprimerUnVoisin(String direction){
        direction = direction.toUpperCase();
        switch (direction){
            case "HAUT": listeVoisin[0] = null; break;
            case "BAS": listeVoisin[1] = null; break;
            case "GAUCHE": listeVoisin[2] = null; break;
            case "DROITE": listeVoisin[3] = null; break;
            default: throw new IllegalArgumentException("Direction invalide");
        }
    }
}