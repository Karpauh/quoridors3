package fr.umontpellier.iut;

import java.util.Objects;

public class Position {
    private int ligne;
    private int colonne;

    private Position posPere;


    public Position(int ligne, int colonne) {
        this.ligne = ligne;
        this.colonne = colonne;
    }

    public Position(Position positionACopier) {
        this.ligne = positionACopier.ligne;
        this.colonne = positionACopier.colonne;
    }

    public void setPosition(int ligne, int colonne) {
        this.ligne = ligne;
        this.colonne = colonne;
    }

    public int getLigne(){
        return ligne;
    }
    public int getColonne(){
        return colonne;
    }

    /*
    //redefinition du equals(), verifie si la ligne et la colonne est la meme que la position du joueur
    * */

    public Position getPosPere() {
        return posPere;
    }

    public void setPosPere(Position posPere) {
        this.posPere = posPere;
    }

    public int remonterArbreParent(){
        if (this.posPere == null) return 0;
        else return posPere.remonterArbreParent() + 1;
    }
    public Position coutOptimal(){
        if (this.posPere.posPere == null) {
            System.out.println(this.ligne+"col"+this.getColonne());
            return this;

        }
        else return posPere.coutOptimal();
    }

    public void setLigne(int ligne) {
        this.ligne = ligne;
    }

    public void setColonne(int colonne) {
        this.colonne = colonne;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return ligne == position.ligne &&
                colonne == position.colonne;
    }

    @Override
    public int hashCode() {
        return Objects.hash(ligne, colonne);
    }

    @Override
    public String toString() {
        return "[" + ligne + ";" + colonne + ']';
    }
}