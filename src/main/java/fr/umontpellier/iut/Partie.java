package fr.umontpellier.iut;


import fr.umontpellier.iut.Momento.PlateauCaretaker;

import java.util.List;

public class Partie {
    private Joueur[] listeJoueurs;
    private int joueurCourant = 0;
    private Plateau plateau;
    private Affichage affichage;
    private PlateauCaretaker plateauCaretaker;


    public Partie() {
        this.affichage = new Affichage();
     }

    public void demarrer() {
        //On recupere la taille du plateau
        int taille = affichage.getIntInput("Taille du plateau");
        while (taille<=0) {
            System.out.println("Taille invalide (doit etre superieur à 0)");
            taille = affichage.getIntInput("Taille du plateau");
        }

        //On initialise la partie
        listeJoueurs = new Joueur[2];




        //1v1 ou 1vIA?
        String choix = affichage.getStringInput("1v1 ou 1vIA ou IAvIA");
        while (!choix.equals("1v1")&&!choix.equals("1vIA")&&!choix.equals("IAvIA")) {
            System.out.println("Choix invalide (1v1; 1vIA; IAvIA)");
            choix = affichage.getStringInput("1v1 ou 1vIA ou IAvIA");
        }
        if (choix.equals("1v1")) {
            listeJoueurs[0] = new Joueur(0, taille/2);
            listeJoueurs[1] = new Joueur(taille-1, taille/2);
        }
        else if (choix.equals("1vIA")) {
            listeJoueurs[0] = new Joueur(0, taille/2);
            listeJoueurs[1] = new IANiv2(taille-1, taille/2, this, 3);
        }
        else {
            listeJoueurs[0] = new IANiv2(0, taille/2, this, 4);
            listeJoueurs[1] = new IANiv2(taille-1, taille/2, this, 3);
        }


        plateau = new Plateau(taille, listeJoueurs);
        //on commence à jouer
        jouer();
    }

    public void jouer() {
        //On affiche le plateau et le joueur


        System.out.println("plateau");
        affichage.afficherPartie(plateau, listeJoueurs);


        System.out.println("Joueur "+(joueurCourant+1));
        //System.out.println("Distance : "+this.plateau.distance2points(listeJoueurs[0].getPosition(),listeJoueurs[1].getPosition()));
        switch (listeJoueurs[joueurCourant].choisirAction(affichage)) {
            case "m" :
                if (listeJoueurs[joueurCourant].getNbMur()>0) {
                    choixMur(); break;
                }
                else {
                    System.out.println("Plus de mur");
                    choixDeplacer(); break;

                }
            case "d" :
                choixDeplacer(); break;
        }



        //On regarde si un joueur a gagne, sinon on change de joueur et on rappelle la fonction jouer
        if(this.estGagnee()){
            System.out.println("Joueur " +(joueurCourant+1) + " a gagne");
        }
        else {
            joueurCourant = getJoueurSuivant(); //On passe au joueur suivant
            jouer();
        }
    }



    private void choixDeplacer() {
        //On recupere la direction pour pouvoir déplacer le joueur

        String pos = "";
        List<Position> posDispo = plateau.positionAtteignable(listeJoueurs[joueurCourant].getPosition(), joueurCourant);
        for (Position p : posDispo){
            pos += p.toString();
        }

        System.out.println("Position disponibles : "+pos);

        Position mouvement = listeJoueurs[joueurCourant].choisirMouvement(affichage);
        while (!posDispo.contains(mouvement)){
            mouvement = listeJoueurs[joueurCourant].choisirMouvement(affichage);
        }
        listeJoueurs[joueurCourant].seDeplacer(mouvement);


    }


    private void choixMur() {
        int ligne;
        int colonne;
        String orientation;
        boolean estValide;
        do {
            //ligne
            System.out.println("Poser mur : Choisir une ligne(0.." + (plateau.getTaille() - 2) + ")");
            ligne = listeJoueurs[joueurCourant].choisirLigneMur(affichage, plateau.getTaille());
            while (ligne < 0 || ligne >= plateau.getTaille()-1) {
                System.out.println("ligne invalide");
                ligne = listeJoueurs[joueurCourant].choisirLigneMur(affichage, plateau.getTaille());
            }
            //colonne
            System.out.println("Poser mur : Choisir une colonne(0.." + (plateau.getTaille() - 2) + ")");
            colonne = listeJoueurs[joueurCourant].choisirColonneMur(affichage, plateau.getTaille());
            while (colonne < 0 || colonne >= plateau.getTaille()-1) {
                System.out.println("colonne invalide");
                colonne = listeJoueurs[joueurCourant].choisirColonneMur(affichage, plateau.getTaille());
            }
            //orientation
            System.out.println("Choix de l'orientation : Vertical(v) ou Horizontal(h)");
            orientation = listeJoueurs[joueurCourant].choisirOrientationMur(affichage);
            while (!orientation.equals("v") && !orientation.equals("h")) {
                System.out.println("fvalide");
                orientation = listeJoueurs[joueurCourant].choisirOrientationMur(affichage);
            }
            estValide = plateau.murEstValide(ligne, colonne, orientation);
            if (!estValide) {
                System.out.println("Mur invalide : veuillez reessayer");
            }
        } while (!estValide);

        Mur m = new Mur(new Position(ligne, colonne), orientation);
        plateau.poserMur(m);
        listeJoueurs[joueurCourant].poserMur();
    }







    //On teste si la ligne du joueur est egale a la ligne ou il doit se trouver pour gagner
    public boolean estGagnee() { return listeJoueurs[0].getPosition().getLigne() >= this.plateau.getTaille()-1 || listeJoueurs[1].getPosition().getLigne() <= 0; }

    public Plateau getPlateau() {
        return plateau;
    }

    public Joueur[] getListeJoueurs() {
        return listeJoueurs;
    }

    public int getJoueurCourant() {
        return joueurCourant;
    }

    public int getJoueurSuivant() {
        return (joueurCourant+1)%2;
    }
}