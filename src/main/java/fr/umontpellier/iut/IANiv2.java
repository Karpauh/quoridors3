package fr.umontpellier.iut;

import fr.umontpellier.iut.Momento.PlateauMemento;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class IANiv2 extends IA{

    private Partie partie;
    private int levelIA;
    private Coup c;

    public IANiv2(int ligne, int col, Partie partie, int levelIA) {
        super(ligne, col, partie);
        this.partie = partie;
        this.levelIA = levelIA;
        this.c = new Coup(new Position(0,0));
    }

        /*
        * Idée globale: on est en dimension N, on génère les plateaux de N+1 on s'arrête à la dimension 2 (pour le moment)
        * Les plateaux N+1 correspondent à l'ensemble des coups possible du joueur courant
        * Les plateaux N+2 correspondent à l'ensemble des coups possible de J2 (après le coup de J1)
        * Au niveau N+2 on regarde la valeur de chaque coup avec une fonction d'évaluation (distToWin joueurCourant - distToWin joueurAdverse)
        * On remonte les coups récursivement avec int parcours(int n) si n < à la valeur du coup étudié on return le coups étudié sinon on retourne n
        * On retourne le coup ayant la meilleure valeur
        * */

        //Génération des plateaux jusqu'à N+2

    public int fonctionEvaluation(Plateau plateau){

        int distanceJCourrant = distToWin(partie.getJoueurCourant(), plateau.getListeJoueurs()[partie.getJoueurCourant()], plateau);

        int distanceJAutre = distToWin((partie.getJoueurSuivant()), plateau.getListeJoueurs()[(partie.getJoueurSuivant())], plateau);
        //System.out.println("courrant : "+distanceJCourrant+" autre j : "+distanceJAutre+" c-a "+(distanceJCourrant-distanceJAutre));

        if(aGagne(plateau.getListeJoueurs()[partie.getJoueurCourant()].getPosition(), partie.getJoueurCourant(), plateau)){

            return Integer.MAX_VALUE;
        }
        if(aGagne(plateau.getListeJoueurs()[partie.getJoueurSuivant()].getPosition(), partie.getJoueurSuivant(), plateau)){

            return Integer.MIN_VALUE;
        }
        return distanceJAutre-distanceJCourrant;
    }

    public int  max(Plateau board, int profondeur){
        //System.out.println("MAX");
        int eval;

        PlateauMemento memento = board.cloner();
        Plateau clone = new Plateau(memento.getCasesDuTableau(), memento.getListeJoueurs(), memento.getListeMurs());

        //Si on demande la profondeur courrante : on effectue directement la fonction d'évaluation
        if(aGagne(clone.getListeJoueurs()[partie.getJoueurCourant()].getPosition(), partie.getJoueurCourant(), board) || aGagne(clone.getListeJoueurs()[partie.getJoueurSuivant()].getPosition(), partie.getJoueurSuivant(), board) || profondeur == 0){
            eval = fonctionEvaluation(board);
            /**
            if (eval == Integer.MAX_VALUE)
                System.out.println("IA GAGNE!!!");
            if  (eval == Integer.MIN_VALUE) {
                System.out.println("IA PERT!!!");
            }
             **/
        }

        else {
            eval = Integer.MIN_VALUE;
            Joueur joueurQuiJoue;
            int idJoueurQuiJoue;

            joueurQuiJoue = clone.getListeJoueurs()[partie.getJoueurCourant()];
            idJoueurQuiJoue = partie.getJoueurCourant();
            //Position posJ = board.getListeJoueurs()[partie.getJoueurCourant()].getPosition();

            for(Coup c : creerListeCoup(joueurQuiJoue, board, idJoueurQuiJoue)){
                //joueurQuiJoue.seDeplacer(posJ);

                clone = new Plateau(memento.getCasesDuTableau(), memento.getListeJoueurs(), memento.getListeMurs());
                joueurQuiJoue = clone.getListeJoueurs()[idJoueurQuiJoue];

                if(c.getType()==0){
                    joueurQuiJoue.seDeplacer(c.getPosition());
                }
                else {
                    clone.poserMur(c.getMur());
                    joueurQuiJoue.poserMur();
                }
                eval = Math.max(eval, min(clone, profondeur-1));
            }
            //board.getListeJoueurs()[partie.getJoueurSuivant()].seDeplacer(posJ);

        }

        return eval;
    }

    public int min(Plateau board, int profondeur){
        //System.out.println("MIN");
        int eval;


        PlateauMemento memento = board.cloner();
        Plateau clone = new Plateau(memento.getCasesDuTableau(), memento.getListeJoueurs(), memento.getListeMurs());

        if(aGagne(clone.getListeJoueurs()[partie.getJoueurCourant()].getPosition(), partie.getJoueurCourant(), board) || aGagne(clone.getListeJoueurs()[partie.getJoueurSuivant()].getPosition(), partie.getJoueurSuivant(), board) || profondeur == 0){
            eval = fonctionEvaluation(board);
            /*
            if (eval == Integer.MAX_VALUE)
                System.out.println("AUTRE GAGNE!!!");
            if  (eval == Integer.MIN_VALUE) {
                System.out.println("AUTRE PERT!!!");
            }
             */
        }

        else {
            eval = Integer.MAX_VALUE;
            Joueur joueurQuiJoue;
            int idJoueurQuiJoue;
            //Position posJ = board.getListeJoueurs()[partie.getJoueurSuivant()].getPosition();

            joueurQuiJoue = clone.getListeJoueurs()[partie.getJoueurSuivant()];
            idJoueurQuiJoue = partie.getJoueurSuivant();


            for(Coup c : creerListeCoup(joueurQuiJoue, board, idJoueurQuiJoue)){
                //joueurQuiJoue.seDeplacer(posJ);

                clone = new Plateau(memento.getCasesDuTableau(), memento.getListeJoueurs(), memento.getListeMurs());
                joueurQuiJoue = clone.getListeJoueurs()[idJoueurQuiJoue];

                if(c.getType()==0){
                    joueurQuiJoue.seDeplacer(c.getPosition());
                }
                else {
                    clone.poserMur(c.getMur());
                    joueurQuiJoue.poserMur();
                }
                eval = Math.min(eval, max(clone, profondeur-1));
            }
            //board.getListeJoueurs()[partie.getJoueurSuivant()].seDeplacer(posJ);

        }

        return eval;
    }

    public int minMax(Plateau board, int profondeur){
        return min(board, profondeur);
    }

    public Coup getMove() {
        Plateau board = partie.getPlateau();
        Coup moveAreturn = new Coup(new Position(0, 0));

        int valeur = Integer.MIN_VALUE;
        int valeurActu = 0;
        //Position posJ = this.getPosition();


        for(Coup c : creerListeCoup(board.getListeJoueurs()[partie.getJoueurCourant()], board, partie.getJoueurCourant())){
            //this.seDeplacer(posJ);
            PlateauMemento memento = board.cloner();
            Plateau clone = new Plateau(memento.getCasesDuTableau(), memento.getListeJoueurs(), memento.getListeMurs());
            if(c.getType()==0){
                clone.getListeJoueurs()[partie.getJoueurCourant()].seDeplacer(c.getPosition());
                System.out.println(c.getPosition().toString());
            }
            else {
                clone.poserMur(c.getMur());
                clone.getListeJoueurs()[partie.getJoueurCourant()].poserMur();
                System.out.println(c.getMur().toString());
            }
            valeurActu = minMax(clone, levelIA);
            System.out.println("oueee "+valeurActu);

            if (valeurActu >= valeur){
                valeur = valeurActu;
                moveAreturn = c;
            }
        }
        //this.seDeplacer(posJ);
        System.out.println("Valeur du coup : "+valeur);
        return moveAreturn;

    }


    public List<Coup> creerListeCoup(Joueur joueur,Plateau plateau, int idJoueur){
        List<Coup> listeareturn = new ArrayList<>();
        for (Mur m :  plateau.getMurPossible(joueur)){
            Coup coup = new Coup(m);
            listeareturn.add(coup);
        }
        for (Position pos : plateau.positionAtteignable(joueur.getPosition(), idJoueur)){
            Coup coup = new Coup(pos);
            listeareturn.add(coup);
        }
        if (listeareturn.isEmpty())
            System.out.println("Coup vide !!!!");
        return listeareturn;
    }

    @Override
    public String choisirAction(Affichage affichage) {
        delayer();

        Coup c = getMove();
        System.out.println("Cout selectionné : "+c.toString());

        String choix;
        this.c = c;
        if (c.getType()==1){
            System.out.println("wall choice"+ c.getMur().toString());
            choix = "m";
        }
        else {choix = "d";
            System.out.println("dep choice "+c.getPosition().toString());
        }

        return choix;
    }

    @Override
    public Position choisirMouvement(Affichage affichage) {
        Position posAreturn = this.c.getPosition();
        return posAreturn;
    }


    @Override
    public int choisirLigneMur(Affichage affichage, int taille) {
        return this.c.getMur().getPosition().getLigne();
    }

    @Override
    public int choisirColonneMur(Affichage affichage, int taille) {

        return this.c.getMur().getPosition().getColonne();
    }

    @Override
    public String choisirOrientationMur(Affichage affichage) {
        //delayer();
        return this.c.getMur().getOrientation().toLowerCase();
    }

}
