package fr.umontpellier.iut;

import fr.umontpellier.iut.Momento.PlateauMemento;

import java.util.*;

public class Plateau {
    private final int taille;
    private Case[][] casesDuTableau;
    private ArrayList<Mur> listeMurs;
    private Joueur[] listeJoueurs;


    /*On construit un plateau de taille par taille, et on lui donne un tableau de Joueur*/
    public Plateau(int taille, Joueur[] joueurs) {
        this.taille = taille;
        this.casesDuTableau = new Case[taille][taille];
        listeMurs = new ArrayList<>();
        this.listeJoueurs = joueurs;
        genererPlateau();
    }

    public Plateau(Case[][] casesDuTableau, Joueur[] joueur, ArrayList<Mur> listeMurs) {
        this.taille = casesDuTableau.length;
        this.casesDuTableau = new Case[taille][taille];
        this.listeJoueurs = joueur;
        this.listeMurs = new ArrayList<>();
        this.listeMurs.addAll(listeMurs);
        copierPlateau(casesDuTableau);
    }

    public void copierPlateau(Case[][] casesACopier) {
        genererPlateau();
        //On parcours toutes les casesACopier pour savoir où sont les mur
        for (int i = 0; i<taille; i++) {
            for (int j = 0; j<taille; j++) {
                Case caseLocale = casesACopier[i][j];
                String[] directions = {"HAUT", "BAS", "GAUCHE", "DROITE"};
                for (String d: directions) {
                    if (caseLocale.getVoisin(d)==null) {
                        casesDuTableau[i][j].supprimerUnVoisin(d);
                    }
                }
            }
        }
    }

    /*
     *Initialise les cases du plateau
     */
    public void genererPlateau() {
        //On cree toutes les cases
        for (int i = 0; i < taille; i++) {
            for (int j = 0; j < taille; j++) {
                casesDuTableau[i][j] = new Case(i, j);
            }
        }

        //On leur donne un voisin
        //Voisin en haut (ne pas prendre la premiere ligne)
        for (int i = 1; i < taille; i++) {
            for (int j = 0; j < taille; j++) {
                casesDuTableau[i][j].setVoisin("haut", casesDuTableau[i-1][j]);
            }
        }

        //Voisin en bas (ne pas prendre la derniere ligne)
        for (int i = 0; i < taille-1; i++) {
            for (int j = 0; j < taille; j++) {
                casesDuTableau[i][j].setVoisin("bas", casesDuTableau[i+1][j]);
            }
        }

        //Voisin à gauche (ne pas prendre la premiere colonne)
        for (int i = 0; i < taille; i++) {
            for (int j = 1; j < taille; j++) {
                casesDuTableau[i][j].setVoisin("gauche", casesDuTableau[i][j-1]);
            }
        }

        //Voisin à droite (ne pas prendre la derniere ligne)
        for (int i = 0; i < taille; i++) {
            for (int j = 0; j < taille-1; j++) {
                casesDuTableau[i][j].setVoisin("droite", casesDuTableau[i][j+1]);
            }
        }
    }

    public Case getCase(Position p) {
        return casesDuTableau[p.getLigne()][p.getColonne()];
    }
    public Case getCase(int i, int j) {
        return casesDuTableau[i][j];
    }
    public int getTaille() {
        return taille;
    }


    //PARTIE MUR

    public void poserMur(Mur m) {
        //position est la case "en haut à droite"
        //préprequis : il n'y as pas de mur déjà placés
        /*vertical (v)     horizontal (h)
          c1  |  c2          c1     c2
              |               -------
          c3  |  c4          c3     c4
         */
        int i = m.getPosition().getLigne();
        int j = m.getPosition().getColonne();
        Case c1 = casesDuTableau[i][j];
        Case c2 = casesDuTableau[i][j+1];
        Case c3 = casesDuTableau[i+1][j];
        Case c4 = casesDuTableau[i+1][j+1];

        switch (m.getOrientation()) {
            case "V": {
                c1.setVoisin("droite", null);
                c2.setVoisin("gauche", null);
                c3.setVoisin("droite", null);
                c4.setVoisin("gauche", null);
                break;
            }
            case "H": {
                c1.setVoisin("bas", null);
                c2.setVoisin("bas", null);
                c3.setVoisin("haut", null);
                c4.setVoisin("haut", null);
                break;
            }
            default: throw new IllegalArgumentException("Orientation invalide");
        }
        listeMurs.add(m);
    }

    public void supprimerMur(Mur m) {
        //position est la case "en haut à droite"
        //préprequis : il n'y as pas de mur déjà placés
        /*vertical (v)     horizontal (h)
          c1  |  c2          c1     c2
              |               -------
          c3  |  c4          c3     c4
         */
        int i = m.getPosition().getLigne();
        int j = m.getPosition().getColonne();
        Case c1 = casesDuTableau[i][j];
        Case c2 = casesDuTableau[i][j+1];
        Case c3 = casesDuTableau[i+1][j];
        Case c4 = casesDuTableau[i+1][j+1];

        switch (m.getOrientation()) {
            case "V": {
                c1.setVoisin("droite", c2);
                c2.setVoisin("gauche", c1);
                c3.setVoisin("droite", c4);
                c4.setVoisin("gauche", c3);
                break;
            }
            case "H": {
                c1.setVoisin("bas", c3);
                c2.setVoisin("bas", c4);
                c3.setVoisin("haut", c1);
                c4.setVoisin("haut", c2);
                break;
            }
            default: throw new IllegalArgumentException("Orientation invalide");
        }
        listeMurs.remove(m);
    }
    private boolean pasDeMur(int i, int j, String orientation) {
            /*vertical (v)          horizontal (h)
              c1  |  c2               c1     c2
                  |                   ---------
              c3  |  c4               c3     c4
             */
        orientation = orientation.toUpperCase();
        switch (orientation) {
            case "V" : {
                //overlapping vertical
                if (listeMurs.contains(new Mur(new Position(i, j), "V"))) return false;
                if (listeMurs.contains(new Mur(new Position(i-1, j), "V"))) return false;
                if (listeMurs.contains(new Mur(new Position(i+1, j), "V"))) return false;
                //overlapping horizontal
                if (listeMurs.contains(new Mur(new Position(i, j), "H"))) return false;
                return true;
            }
            case "H" : {
                //overlapping vertical
                if (listeMurs.contains(new Mur(new Position(i, j), "V"))) return false;
                //overlapping horizontal
                if (listeMurs.contains(new Mur(new Position(i, j), "H"))) return false;
                if (listeMurs.contains(new Mur(new Position(i, j-1), "H"))) return false;
                if (listeMurs.contains(new Mur(new Position(i, j+1), "H"))) return false;
                return true;
            }
            default: throw new IllegalArgumentException("Orientation invalide");
        }
    }
    public boolean murEstValide(int i, int j, String orientation) {
        return (i >= 0 && i < taille-1)&&(j >= 0 && j < taille-1) && pasDeMur(i, j, orientation) && murClasse(i, j, orientation);
    }

    public boolean murEstValide(Mur m) {
        return murEstValide(m.getPosition().getLigne(), m.getPosition().getColonne(), m.getOrientation());
    }



    private boolean murClasse(int i, int j, String orientation) {
        poserMur(new Mur(new Position(i, j), orientation));
        boolean b = plateauValide();
        supprimerMur(new Mur(new Position(i, j), orientation));
        return b;
    }

    public ArrayList<Mur> getMurPossible(Joueur joueur){
        ArrayList<Mur> listeDesMurs = new ArrayList<>();
        int i, j;
        Mur m;
        //On va tester tout les coups possibles pour avoir les valides
        if (joueur.getNbMur()>0) {
            for (i = 0; i < this.getTaille(); i++) {
                for (j = 0; j < this.getTaille(); j++) {
                    if (murEstValide(i, j, "h")) {
                        m = new Mur(new Position(i, j), "h");
                        listeDesMurs.add(m);

                    }
                    if (murEstValide(i, j, "v")) {
                        m = new Mur(new Position(i, j), "v");
                        listeDesMurs.add(m);
                    }
                }

            }
        }
        return  listeDesMurs;
    }


    //PARTIE DEPLACEMENT
    public List<Position> directionsPossibles(Position p){
        LinkedList<Position> positions = new LinkedList<>();
        String[] directions = {"HAUT", "BAS", "GAUCHE", "DROITE"};
        for (String dir : directions){
            if (getCase(p).getVoisin(dir)!=null){
                positions.add(getCase(p).getVoisin(dir).getPosition());
            }
        }
        return positions;
    }
    public List<Position> directionsPossiblesSaut(Position p, int joueur, String direction){
        LinkedList<Position> positions = new LinkedList<>();
        if(getCase(p).getVoisin(direction)!=null){
            positions.add(getCase(p).getVoisin(direction).getPosition());
            return positions;
        }
        if(joueur == 0&&p.getLigne()==taille-1 || joueur == 1&&p.getLigne()==0){
            positions.add(p);
            return positions;
        }

        String[] directions = {"HAUT", "BAS", "GAUCHE", "DROITE"};
        for (String dir : directions){
            if (getCase(p).getVoisin(dir)!=null&&!getCase(p).getVoisin(dir).getPosition().equals(listeJoueurs[joueur].getPosition())){
                positions.add(getCase(p).getVoisin(dir).getPosition());
            }
        }
        return positions;
    }


    public List<Position> positionAtteignable(Position p, int joueur) {
        ArrayList<Position> returnList = new ArrayList<>();
        for (Position pos : directionsPossibles(p)) {
            if (pos.equals(listeJoueurs[(joueur + 1) % 2].getPosition())) {
                for (Position pos2 : directionsPossiblesSaut(pos, joueur, directionPos(pos, p))) {
                    returnList.add(new Position(pos2));
                }
            }
            else {
                returnList.add(new Position(pos));
            }
        }
        return returnList;
    }


    public String directionPos(Position pos1, Position pos2) {
        //pré-requis : les positions sont "collées"
        if (pos2.getLigne()>pos1.getLigne()) {
            return "HAUT";
        }
        if (pos2.getLigne()<pos1.getLigne()) {
            return "BAS";
        }
        if (pos2.getColonne()>pos1.getColonne()) {
            return "GAUCHE";
        }
        return "DROITE";
    }








    public boolean plateauValide() {
        //Il s'agit de la classe de conexitée
        LinkedList<Case> visitee = new LinkedList<>();
        Queue<Case> enCour = new LinkedList<>();
        String[] directions = {"HAUT", "BAS", "GAUCHE", "DROITE"};
        boolean j1connexe = false;
        boolean j2connexe = false;
        for(int i = 0;i<2;i++) {
            Joueur j = listeJoueurs[i];
            visitee.clear();
            enCour.clear();
            visitee.add(casesDuTableau[j.getPosition().getLigne()][j.getPosition().getColonne()]);
            enCour.add(casesDuTableau[j.getPosition().getLigne()][j.getPosition().getColonne()]);
            Case actuelle;
            Case temp;
            while (!enCour.isEmpty()) {
                actuelle = enCour.remove();
                for (String dir : directions) {
                    temp = actuelle.getVoisin(dir);
                    if (temp != null && !visitee.contains(temp)) {
                        visitee.add(temp);
                        enCour.add(temp);
                        if(i==0&&temp.getPosition().getLigne()==this.taille-1){
                            j1connexe = true;
                        }
                        if(i==1&&temp.getPosition().getLigne()==0){
                            j2connexe = true;
                        }
                    }
                }
            }
        }

        return j1connexe&&j2connexe;

    }

    public boolean positionEstDansPlateau(Position p) {
        return (p.getLigne()<0 || p.getLigne()>=taille || p.getColonne()<0 || p.getColonne()>=taille);
    }

    public void clearParents(){
        for (int i = 0;i<taille;i++){
            for (int j = 0;j<taille;j++){
                getCase(i,j).getPosition().setPosPere(null);
            }
        }
    }


    public PlateauMemento cloner() {
        return new PlateauMemento(this.casesDuTableau, this.listeJoueurs, this.listeMurs);
    }

    public Joueur[] getListeJoueurs() {
        return listeJoueurs;
    }
}