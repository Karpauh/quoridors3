package fr.umontpellier.iut;

import fr.umontpellier.iut.Joueur;
import fr.umontpellier.iut.Partie;
import fr.umontpellier.iut.Position;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.awt.Robot;

public abstract class IA extends Joueur {
    protected Partie partie;

    public IA(int ligne, int col, Partie partie) {
        super(ligne, col);
        this.partie = partie;
    }
    public void delayer() {
        try {
            Thread.sleep(1000);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public List<Position> directionsPossibles(Position p, Plateau plateau){
        LinkedList<Position> positions = new LinkedList<>();
        String[] directions = {"HAUT", "BAS", "GAUCHE", "DROITE"};
        for (String dir : directions){
            if (plateau.getCase(p).getVoisin(dir)!=null){
                positions.add(plateau.getCase(p).getVoisin(dir).getPosition());
            }
        }
        return positions;
    }
    public List<Position> directionsPossiblesSaut(Position p, int joueur, String direction, Plateau plateau){
        LinkedList<Position> positions = new LinkedList<>();
        if(plateau.getCase(p).getVoisin(direction)!=null){
            positions.add(plateau.getCase(p).getVoisin(direction).getPosition());
            return positions;
        }
        if(joueur == 0&&p.getLigne()==plateau.getTaille()-1 || joueur == 1&&p.getLigne()==0){
            positions.add(p);
            return positions;
        }

        String[] directions = {"HAUT", "BAS", "GAUCHE", "DROITE"};
        for (String dir : directions){
            if (plateau.getCase(p).getVoisin(dir)!=null&&!plateau.getCase(p).getVoisin(dir).getPosition().equals(plateau.getListeJoueurs()[joueur].getPosition())){
                positions.add(plateau.getCase(p).getVoisin(dir).getPosition());
            }
        }
        return positions;
    }
    //Position atteinte lorsque le joueur peut gagner avec le moins de coups possibles (sert surtout à initialiser les posPere pour remonter l'arbre dans distToWin)
    public Position posAtteinte(Position p1, int joueur, Plateau plateau){
        plateau.clearParents();
        LinkedList<Position> visitee = new LinkedList<>();
        Queue<Position> enCour = new LinkedList<>();
        Position actu = p1;
        visitee.add(actu);
        enCour.add(actu);
        for(Position pos : directionsPossibles(actu, plateau)){
            if (!visitee.contains(pos)) {
                visitee.add(pos);


                if (pos.equals(plateau.getListeJoueurs()[(joueur + 1) % 2].getPosition())) {
                    for (Position pos2 : directionsPossiblesSaut(pos, joueur, directionPos(pos, p1, plateau), plateau)){
                        pos = new Position(pos2);
                        pos.setPosPere(actu);
                        visitee.add(pos);
                        enCour.add(pos);
                    }


                }
                else   {
                    pos.setPosPere(actu);
                    enCour.add(pos);
                }

                if (aGagne(pos, joueur, plateau)) {
                    actu = pos;
                    return actu;
                }
            }
        }


        while (!enCour.isEmpty()&&!aGagne(actu,joueur, plateau)){
            actu = enCour.remove();

            for(Position pos : directionsPossibles(actu, plateau)){
                if(!visitee.contains(pos)){
                    pos.setPosPere(actu);
                    visitee.add(pos);
                    enCour.add(pos);
                    if(aGagne(pos,joueur, plateau)){
                        actu = pos;
                        break;
                    }
                }
            }
        }
        return actu;

    }
    //Nombre de coups que le joueur doit faire pour gagner
    public int distToWin(int idjoueur, Joueur joueur, Plateau plateau){
        Position test = posAtteinte(joueur.getPosition(),idjoueur, plateau);
        int haha =  test.remonterArbreParent();
        return haha;
    }

    public Position posaaller(int idjoueur, Joueur joueur, Plateau plateau){
        Position test = posAtteinte(joueur.getPosition(),idjoueur, plateau);
        return test.coutOptimal();
    }
    //Renvoie true si la pos du joueur est gagnante
    public boolean aGagne(Position position, int idJoueur, Plateau plateau){
        if (idJoueur == 0&&position.getLigne()>=plateau.getTaille()-1){
            return true;
        }
        else if (idJoueur == 1&&position.getLigne()<=0){
            return true;
        }
        else return false;
    }

    public String directionPos(Position pos1, Position pos2, Plateau plateau) {
        //pré-requis : les positions sont "collées"
        if(pos1.getLigne()!=pos2.getLigne()&&pos1.getColonne()!=pos2.getColonne()){
            pos1 = plateau.getListeJoueurs()[partie.getJoueurSuivant()].getPosition();
        }

        return directionPosbis(pos1, pos2);
    }
    public String directionPosbis(Position pos1, Position pos2) {
        //pré-requis : les positions sont "collées"
        if (pos2.getLigne()>pos1.getLigne()) {
            return "HAUT";
        }
        if (pos2.getLigne()<pos1.getLigne()) {
            return "BAS";
        }
        if (pos2.getColonne()>pos1.getColonne()) {
            return "GAUCHE";
        }
        return "DROITE";
    }
}
