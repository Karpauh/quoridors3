package fr.umontpellier.iut.Momento;

import fr.umontpellier.iut.Case;
import fr.umontpellier.iut.Joueur;
import fr.umontpellier.iut.Mur;

import java.util.ArrayList;

public class PlateauMemento {
    private Case[][] casesDuTableau;
    private Joueur[] listeJoueurs;
    private ArrayList<Mur> listeMurs;


    public PlateauMemento(Case[][] casesDuTableau, Joueur[] listeJoueurs, ArrayList<Mur> listeMurs) {
        int taille = casesDuTableau.length;
        this.listeJoueurs = new Joueur[listeJoueurs.length];
        for (int i =0; i< listeJoueurs.length; i++) {
            this.listeJoueurs[i] = new Joueur(listeJoueurs[i].getPosition(), listeJoueurs[i].getNbMur());
        }
        this.casesDuTableau = new Case[taille][taille];
        this.listeMurs = new ArrayList<>();
        this.listeMurs.addAll(listeMurs);
        genererPlateau(taille, casesDuTableau);
    }

    public void genererPlateau(int taille, Case[][] casesDuTableau) {
        //On cree toutes les cases
        for (int i = 0; i < taille; i++) {
            for (int j = 0; j < taille; j++) {
                this.casesDuTableau[i][j] = new Case(i, j);
            }
        }

        //On leur donne un voisin
        //Voisin en haut (ne pas prendre la premiere ligne)
        for (int i = 1; i < taille; i++) {
            for (int j = 0; j < taille; j++) {
                if(!casesDuTableau[i][j].aUnMur("haut")){
                    this.casesDuTableau[i][j].setVoisin("haut", this.casesDuTableau[i-1][j]);
                }
            }
        }

        //Voisin en bas (ne pas prendre la derniere ligne)
        for (int i = 0; i < taille-1; i++) {
            for (int j =    0; j < taille; j++) {
                if(!casesDuTableau[i][j].aUnMur("bas")){
                    this.casesDuTableau[i][j].setVoisin("bas", this.casesDuTableau[i+1][j]);
                }
            }
        }

        //Voisin à gauche (ne pas prendre la premiere colonne)
        for (int i = 0; i < taille; i++) {
            for (int j = 1; j < taille; j++) {
                if(!casesDuTableau[i][j].aUnMur("gauche")){
                    this.casesDuTableau[i][j].setVoisin("gauche", this.casesDuTableau[i][j-1]);
                }
            }
        }

        //Voisin à droite (ne pas prendre la derniere ligne)
        for (int i = 0; i < taille; i++) {
            for (int j = 0; j < taille-1; j++) {
                if(!casesDuTableau[i][j].aUnMur("droite")){
                    this.casesDuTableau[i][j].setVoisin("droite", this.casesDuTableau[i][j+1]);
                }
            }
        }
    }

    public Case[][] getCasesDuTableau() {
        return casesDuTableau;
    }

    public Joueur[] getListeJoueurs() {
        return listeJoueurs;
    }

    public ArrayList<Mur> getListeMurs() {
        return listeMurs;
    }
}
