package fr.umontpellier.iut.Momento;


import java.util.ArrayDeque;
import java.util.Deque;

public class PlateauCaretaker {

    final Deque<PlateauMemento> mementos = new ArrayDeque<>();

    public PlateauMemento getMemento() {


        PlateauMemento plateauMemento= mementos.pop();

        return plateauMemento;

    }


    public void addMemento(PlateauMemento memento) {

        mementos.push(memento);

    }

    public boolean debutPartie(){
        return mementos.isEmpty();
    }
}