package fr.umontpellier.iut;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.LinkedHashMap;
import java.util.Scanner;

public class Affichage {
    private Scanner sc;

    public Affichage() {
        sc = new Scanner(System.in);
    }

    public int getIntInput(String text) {
        //retourne le prochain int saisi à récupérer
        sc = new Scanner(System.in);
        System.out.print(text+" : ");
        try {
            return sc.nextInt();
        }
        catch (InputMismatchException e) {
            System.out.println("L'entrée n'est pas un entier, veuillez recommencer");
            return getIntInput(text);
        }
    }

    public String getStringInput(String text) {
        //retourne le prochain String saisi à récupérer
        sc = new Scanner(System.in);
        System.out.print(text+" : ");
        return sc.next();
    }

    public Scanner getSc() {
        return sc;
    }

    //affiche l'etat actuel de la partie
    public void afficherPartie(Plateau plateau, Joueur[] listeJoueurs){
        StringBuilder partie = new StringBuilder();  //Creation du tableau a afficher (utilisation d'un StringBuilder)

        LinkedHashMap<Position, String> listePositionJoueur = new LinkedHashMap<>(); //Creation de la Map<Position, pseudoJoueur> = assigne la position a un joueur
        int x = 0; //Variable qui permet de nommer les joueurs
        System.out.println(listeJoueurs.length);
        for (Joueur j : listeJoueurs){
            x +=1;
            listePositionJoueur.put(j.getPosition(), "J"+x); //Remplissage de Map<Position, pseudoJoueur>
        }

        //construction du plateau ligne par ligne
        StringBuilder lignePleine;
        StringBuilder ligneCreuse;
        //Pour chacune des cases :
        for(int i = 0; i<plateau.getTaille(); i++){
            lignePleine = new StringBuilder();
            ligneCreuse = new StringBuilder();
            //on initialise la ligne creuse avec des espaces
            for (int c = 0; c < plateau.getTaille(); c++) {
                ligneCreuse.append("    ");
            }
            for(int j = 0; j<plateau.getTaille(); j++){
                //S'il y a un joueur sur la case, on l'affiche. Sinon, on affiche " * "
                lignePleine.append(" ").append(listePositionJoueur.getOrDefault(plateau.getCase(i, j).getPosition(), "* ")).append(" ");

                //Si il y a un mur à droite
                if (plateau.getCase(i, j).aUnMur("droite")) {
                    lignePleine.deleteCharAt(j*4+3);
                    lignePleine.append("|");
                }
                //Si il y a un mur en bas
                if (plateau.getCase(i, j).aUnMur("bas")) {
                    ligneCreuse.setCharAt(j*4+1, '-');
                }
            }
            partie.append(lignePleine);
            partie.append("\n");
            partie.append(ligneCreuse);
            partie.append("\n");
        }

        System.out.println(partie.toString());  //On affiche le plateau
    }

    public void afficher(String texte) {
        System.out.println(texte);
    }
}