package fr.umontpellier.iut;

import java.util.ArrayList;
import java.util.Objects;

public class Mur {
    private Position position;
    private String orientation;

    public Mur(Position position, String orientation) {
        this.position = position;
        this.orientation = orientation.toUpperCase();
    }

    public Position getPosition() {
        return position;
    }

    public String getOrientation() {
        return orientation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Mur)) return false;
        Mur mur = (Mur) o;
        return Objects.equals(position, mur.position) &&
                Objects.equals(orientation, mur.orientation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, orientation);
    }

    @Override
    public String toString() {
        return "Mur{" +
                "position=" + position +
                ", orientation='" + orientation + '\'' +
                '}';
    }
}
