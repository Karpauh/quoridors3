package fr.umontpellier.iut;

public class Coup {

    private int type;
    private Mur mur;
    private Position position;

    public Coup(Mur m){
        this.mur = m;
        this.type = 1;
    }

    public Coup(Position p){
        this.position = p;
        this.type = 0;
    }

    public int getType() {
        return type;
    }

    public Mur getMur() {
        return mur;
    }

    public Position getPosition() {
        return position;
    }

    @Override
    public String toString() {
        return "Coup{" +
                "type=" + type +
                ", mur=" + mur +
                ", position=" + position +
                '}';
    }
}
